package lw.homework.config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan(basePackages = {"lw.homework.pageobjects", "lw.homework.tests"})
public class Config
{
    @Bean
    @Profile("firefox")
    public WebDriver getFirefoxDriver()
    {
        return new FirefoxDriver();
    }

    @Bean
    @Profile("chrome")
    public WebDriver getChromeDriver()
    {
        return new ChromeDriver();
    }

    @Bean
    @Autowired
    public WebDriverWait getWebDriverWait(WebDriver driver)
    {
        return new WebDriverWait(driver, 10);
    }

}
