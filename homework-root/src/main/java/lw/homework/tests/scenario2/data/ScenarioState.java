package lw.homework.tests.scenario2.data;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ScenarioState
{
    private List<Course> coursesOnHomePage;
    private List<Course> coursesOnCoursesPage;

    public List<Course> getCoursesOnHomePage()
    {
        return coursesOnHomePage;
    }

    public ScenarioState setCoursesOnHomePage(List<Course> coursesOnHomePage)
    {
        this.coursesOnHomePage = coursesOnHomePage;
        return this;
    }

    public List<Course> getCoursesOnCoursesPage()
    {
        return coursesOnCoursesPage;
    }

    public ScenarioState setCoursesOnCoursesPage(List<Course> coursesOnCoursesPage)
    {
        this.coursesOnCoursesPage = coursesOnCoursesPage;
        return this;
    }
}
