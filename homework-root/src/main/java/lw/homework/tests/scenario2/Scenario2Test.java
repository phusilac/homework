package lw.homework.tests.scenario2;

import lw.homework.pageobjects.scenario2.home.HomePage;
import lw.homework.tests.BaseTest;
import lw.homework.tests.scenario2.data.Course;
import lw.homework.tests.scenario2.data.ScenarioState;
import org.assertj.core.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.List;
import java.util.stream.Collectors;

public class Scenario2Test extends BaseTest
{

    @Autowired
    private ScenarioState scenarioState;
    @Autowired
    private HomePage homePage;

    @Test
    public void scenario2()
    {
        getCoursesFromHomePage();
        getCoursesFromCoursesPage();
        verifyCourseListMatches();
    }

    private void verifyCourseListMatches()
    {
        Assertions
            .assertThat(scenarioState.getCoursesOnHomePage())
            .describedAs("Course list on home page and courses page should match")
            .containsExactlyInAnyOrderElementsOf(scenarioState.getCoursesOnCoursesPage());
    }

    private void getCoursesFromCoursesPage()
    {
        List<Course> stringList = homePage.viewAllUpcomingCourses().get()
                                      .getCourses()
                                      .stream()
                                      .map(Course::new)
                                      .collect(Collectors.toList());
        scenarioState.setCoursesOnCoursesPage(stringList);
    }

    private void getCoursesFromHomePage()
    {
        homePage.open();
        List<Course> upcomingCourses = homePage.get().getUpcomingCourses().stream()
                                           .map(Course::new)
                                           .collect(Collectors.toList());
        scenarioState.setCoursesOnHomePage(upcomingCourses);
    }


}
