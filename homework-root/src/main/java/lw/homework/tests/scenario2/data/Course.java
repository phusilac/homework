package lw.homework.tests.scenario2.data;

import lw.homework.pageobjects.scenario2.home.HomePageCourse;
import lw.homework.pageobjects.scenario2.upcomingcourses.UpcomingCourseDetails;

import java.util.Objects;

public class Course
{
    private String name;
    private String date;
    private String location;

    public Course(UpcomingCourseDetails courseDetails)
    {
        this(courseDetails.getName(), courseDetails.getStartDate(), courseDetails.getLocation());
    }

    public Course(HomePageCourse course)
    {
        this(course.getName(), course.getDate(), course.getLocation());
    }

    public Course(String name, String date, String location)
    {
        this.name = name;
        this.date = date;
        this.location = location;
    }

    public String getName()
    {
        return name;
    }

    public String getDate()
    {
        return date;
    }

    public String getLocation()
    {
        return location;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(name, course.name) &&
                   Objects.equals(date, course.date) &&
                   Objects.equals(location, course.location);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name, date, location);
    }
}
