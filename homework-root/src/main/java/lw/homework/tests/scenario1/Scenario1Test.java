package lw.homework.tests.scenario1;

import lw.homework.pageobjects.scenario1.GoogleResultPage;
import lw.homework.pageobjects.scenario1.GoogleSearchPage;
import lw.homework.tests.BaseTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Scenario1Test extends BaseTest
{

    //public static final String QUERY = "microsoft";
    //public static final String EXPECTED_RESULT = "Microsoft - Official Home Page";
    //public static final String RESULT_QUERY = "surface";
    //public static final String EXPECTED_RESULT_QUERY = "surface site:microsoft.com";
    //public static final String EXPECTED_RESULT_QUERY_LINK = "https://www.microsoft.com/en-us/surface";

    private static final String QUERY = "stack overflow";
    private static final String EXPECTED_RESULT = "Stack Overflow - Where Developers Learn, Share, & Build Careers";
    private static final String RESULT_QUERY = "about";
    private static final String EXPECTED_RESULT_QUERY = "about site:stackoverflow.com";
    private static final String EXPECTED_RESULT_QUERY_LINK = "https://stackoverflow.com/company";

    @Autowired
    private GoogleSearchPage googleSearchPage;
    @Autowired
    private GoogleResultPage googleResultPage;

    @Test
    public void scenario1()
    {
        searchFor(QUERY);
        verifyResultIsPresentWithSearchBar(EXPECTED_RESULT);

        queryResult(EXPECTED_RESULT, RESULT_QUERY);
        verifyQueryIs(EXPECTED_RESULT_QUERY);
        verifyResultPageContainsLink(EXPECTED_RESULT_QUERY_LINK);
    }

    private void verifyResultPageContainsLink(String expectedResultQueryLink)
    {
        boolean hasLinkWithURL = googleResultPage.hasLinkWithURL(expectedResultQueryLink);
        assertThat(hasLinkWithURL)
            .describedAs("Page should contain expected link: %s", expectedResultQueryLink)
            .isTrue();
    }

    private void verifyQueryIs(String expectedResultQuery)
    {
        String searchBarQuery = googleResultPage.get().getSearchBarQuery();
        assertThat(searchBarQuery)
            .describedAs("Search bar should contain expected query: %s", expectedResultQuery)
            .isEqualTo(expectedResultQuery);
    }

    private void queryResult(String expectedResult, String resultQuery)
    {
        googleResultPage.get().searchResultFor(expectedResult, resultQuery);
    }

    private void verifyResultIsPresentWithSearchBar(String expectedResult)
    {
        boolean resultFound = googleResultPage.get().isResultWithSearchBarPresent(expectedResult);
        assertThat(resultFound)
            .describedAs("Expecting search result to have a search bar")
            .isTrue();
    }

    private void searchFor(String query)
    {
        googleSearchPage.open();
        googleSearchPage.get().searchFor(query);
    }
}
