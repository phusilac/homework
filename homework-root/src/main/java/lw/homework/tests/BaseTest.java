package lw.homework.tests;

import lw.homework.config.Config;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterSuite;

@ContextConfiguration(classes = {Config.class})
public class BaseTest extends AbstractTestNGSpringContextTests
{

    @Autowired
    private WebDriver driver;

    @AfterSuite(alwaysRun = true)
    public void killDriver()
    {
        driver.quit();
    }

}
