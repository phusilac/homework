package lw.homework.pageobjects.scenario1;

import lw.homework.pageobjects.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

@PageObject
public class GoogleSearchPage extends LoadableComponent<GoogleSearchPage>
{
    @FindBy(name = "q")
    private WebElement searchInput;
    @FindBy(name = "btnK")
    private WebElement searchButton;

    @Autowired
    private WebDriverWait wait;
    @Autowired
    private WebDriver driver;

    public void searchFor(String query)
    {
        wait.until(visibilityOf(searchInput)).sendKeys(query);
        wait.until(elementToBeClickable(searchButton)).click();
    }

    @Override
    protected void load()
    {
        open();
    }

    public void open()
    {
        driver.get("http://google.us");
    }

    @Override
    protected void isLoaded() throws Error
    {
        assertThatCode(() -> searchInput.isDisplayed()).doesNotThrowAnyException();
        assertThat(searchInput.isDisplayed()).isTrue();
    }
}
