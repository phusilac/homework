package lw.homework.pageobjects.scenario1;

import lw.homework.pageobjects.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

@PageObject
public class GoogleResultPage extends LoadableComponent<GoogleResultPage>
{
    @FindBy(name = "q")
    private WebElement searchInput;

    @FindBy(id = "resultStats")
    private WebElement resultStats;

    @FindBy(id = "rso")
    private WebElement searchResults;

    @FindBy(tagName = "a")
    private List<WebElement> allLinks;

    @Autowired
    private WebDriverWait wait;
    @Autowired
    private WebDriver driver;

    public boolean isResultWithSearchBarPresent(String result)
    {
        return driver.findElements(getSearchBarForResult(result)).size() > 0;
    }

    public void searchResultFor(String result, String query)
    {
        WebElement searchBarElement = wait.until(visibilityOfElementLocated(getSearchBarForResult(result)));
        searchBarElement.sendKeys(query);
        searchBarElement.findElement(By.xpath("./parent::form/button[@type='submit']")).click();

    }

    private By getSearchBarForResult(String resultText)
    {
        String resultSearchBarXPath = String.format(".//h3[text()='%s']/ancestor::div[@class='g']//input[@id='nqsbq']", resultText);
        return By.xpath(resultSearchBarXPath);
    }

    @Override
    protected void load()
    {
    }

    @Override
    protected void isLoaded() throws Error
    {
        assertThatCode(() -> wait.until(visibilityOf(resultStats))).doesNotThrowAnyException();
    }

    public String getSearchBarQuery()
    {
        return searchInput.getAttribute("value");
    }

    public boolean hasLinkWithURL(String expectedResultQueryLink)
    {
        String linkXPath = String.format(".//a//cite[text()='%s']", expectedResultQueryLink);
        return driver.findElements(By.xpath(linkXPath)).size() > 0;
    }
}
