package lw.homework.pageobjects.scenario2.upcomingcourses;

import lw.homework.pageobjects.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThatCode;

@PageObject
public class UpcomingCoursesPage extends LoadableComponent<UpcomingCoursesPage>
{

    @FindBy(tagName = "article")
    private List<WebElement> courses;

    @Autowired
    private WebDriverWait wait;

    @Override
    protected void load()
    {
    }

    public List<UpcomingCourseDetails> getCourses()
    {
        return courses.stream()
                   .map(UpcomingCourseDetails::new)
                   .collect(Collectors.toList());
    }

    @Override
    protected void isLoaded() throws Error
    {
        assertThatCode(() -> wait.until(driver -> courses.size() > 0))
            .doesNotThrowAnyException();
    }
}
