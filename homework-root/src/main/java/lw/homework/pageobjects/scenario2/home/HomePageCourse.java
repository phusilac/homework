package lw.homework.pageobjects.scenario2.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WrapsElement;

public class HomePageCourse implements WrapsElement
{
    private WebElement element;

    public HomePageCourse(WebElement element)
    {
        this.element = element;
    }

    @Override
    public WebElement getWrappedElement()
    {
        return element;
    }

    public String getName()
    {
        return element.findElement(By.tagName("a")).getText();
    }

    public String getDate()
    {
        return element.getText()
                   .replaceFirst(getName(), "")
                   .replaceFirst(getLocation(), "")
                   .replaceAll(" - ", "")
                   .replaceFirst("^(\\w{3})\\w*( .*)", "$1$2");

    }

    public String getLocation()
    {
        return element.getText().split(" - \\w+ \\d+, \\d+ - ")[1];
    }

}
