package lw.homework.pageobjects.scenario2.upcomingcourses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WrapsElement;

public class UpcomingCourseDetails implements WrapsElement
{
    private WebElement element;

    @Override
    public WebElement getWrappedElement()
    {
        return element;
    }

    public UpcomingCourseDetails(WebElement element)
    {
        this.element = element;
    }

    public String getStartDate()
    {
        return element
                   .findElement(By.xpath(".//div[text()='Date']/parent::div"))
                   .getText()
                   .replaceFirst("-\\d+,", ",")
                   .replaceAll("Date", "")
                   .replaceAll("\n", "");
    }

    public String getLocation()
    {
        return element
                   .findElement(By.xpath(".//div[text()='Location']/parent::div"))
                   .getText()
                   .replaceAll("Location", "")
                   .replaceAll("\n", "");
    }

    public String getName()
    {
        return element.findElement(By.xpath(".//div[text()='Type of Course']/following-sibling::div")).getText();
    }

}
