package lw.homework.pageobjects.scenario2.home;

import lw.homework.pageobjects.PageObject;
import lw.homework.pageobjects.scenario2.upcomingcourses.UpcomingCoursesPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThatCode;

@PageObject
public class HomePage extends LoadableComponent<HomePage>
{
    @FindBy(xpath = ".//h2[text()='Upcoming Courses Taught by Andy Brandt']/following-sibling::ul[1]/li")
    private List<WebElement> courses;

    @FindBy(partialLinkText = "View all upcoming courses by Andy Brandt")
    private WebElement allCoursesLink;


    @Autowired
    private WebDriver driver;
    @Autowired
    private WebDriverWait wait;
    @Autowired
    private UpcomingCoursesPage upcomingCoursesPage;

    public List<HomePageCourse> getUpcomingCourses()
    {
        return courses.stream()
                   .map(HomePageCourse::new)
                   .collect(Collectors.toList());
    }

    public UpcomingCoursesPage viewAllUpcomingCourses()
    {
        driver.get(allCoursesLink.getAttribute("href"));
        return upcomingCoursesPage;
    }

    public HomePage open()
    {
        driver.get("https://www.scrum.org/andy-brandt");
        return this;
    }

    @Override
    protected void load()
    {
        open();
    }

    @Override
    protected void isLoaded() throws Error
    {
        assertThatCode(() -> wait.until(ExpectedConditions.visibilityOf(allCoursesLink))).doesNotThrowAnyException();
    }
}
